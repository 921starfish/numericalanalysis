package n4;

public class From2To10 {

	public static void main(String[] args) {
		int[] z = { 1, 1, 1, 0, 1, 1, 0, 1, 0 };
		int[] y = { 0, 1, 1, 1 };
		System.out.println(Horner(z, y));

	}

	public static double Horner(int[] z, int[] y) {

		int seisuubu = 1;
		double syousuubu = 1d;

		for (int k = 1; k < z.length; k++) {
			seisuubu = seisuubu * 2 + z[k];
		}

		for (int k = y.length - 2; k >= 0; k--) {
			syousuubu = syousuubu * 0.5 + y[k];
		}
		syousuubu = syousuubu * 0.5;

		if (syousuubu >= 1) {
			System.out.print("小数部が1より大きいです。");
			return 0;
		}

		return seisuubu + syousuubu;
	}
}
