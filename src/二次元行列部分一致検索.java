import java.io.BufferedReader;
import java.io.InputStreamReader;

public class 二次元行列部分一致検索 {
	public static void main(String[] args) throws Exception {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int N = Integer.parseInt(br.readLine());
		String[][] a = new String[N][N];
		for (int i = 0; i < N; i++) {
			a[i] = br.readLine().split(" ");
		}
		int M = Integer.parseInt(br.readLine());
		String[][] b = new String[M][M];
		for (int i = 0; i < M; i++) {
			b[i] = br.readLine().split(" ");
		}
		br.close();

		int[][] nyuryoku = new int[N][N];
		for (int i = 0; i < N; i++) {
			for (int j = 0; j < N; j++) {
				nyuryoku[i][j] = Integer.parseInt(a[i][j]);
			}
		}

		int[][] pattern = new int[M][M];
		for (int i = 0; i < M; i++) {
			for (int j = 0; j < M; j++) {
				pattern[i][j] = Integer.parseInt(b[i][j]);
			}
		}

		// int N = 4;
		// int M = 2;
		// int[][] nyuryoku = { { 0, 0, 0, 0 }, { 0, 0, 1, 1 }, { 0, 0, 1, 1 },
		// { 0, 0, 0, 0 } };
		// int[][] pattern = { { 1, 1 }, { 1, 1 } };

		boolean c = true;

		for (int k = 0; k < N - M + 1; k++) {
			for (int l = 0; l < N - M + 1; l++) {
				for (int i = k; i < M + k; i++) {
					for (int j = l; j < M + l; j++) {
						if (nyuryoku[i][j] != pattern[i - k][j - l]) {
							c = false;
						}
					}
				}
				if (c) {
					System.out.println(k + " " + l);
				}
				c = true;
			}
		}
	}
}
