package forKoukiChukanTest;

import static util.Calc.*;

public class CalculationForKouki {

	public static void main(String[] args) {
		// このクラスでは、Calcクラスを呼び出すだけでできるような計算を行う。
		double[][] A = new double[50][50];
		for (int i = 0; i < 50; i++) {
			for (int j = 0; j < 50; j++) {
				A[i][j] = (i + 1) + (j + 1);
			}
		}

		System.out.println(matNormFrobenius(A));
		// System.out.println(2.75E3);

		System.out.println(1.0E7);

		double[] x = new double[7];
		for (int i = 0; i < x.length; i++) {
			x[i] = Math.pow(i + 1, i + 1);
		}
		System.out.println(vecNorm2(x));

		double[] x2 = new double[8];
		for (int i = 0; i < x2.length; i++) {
			x2[i] = Math.pow(i + 1, i + 1);
		}
		System.out.println(vecNorm2(x2));

		double g = (-1.1) * 3.3000000000000007 * (-131.99999999999997);
		System.out.println(g);
	}
}
