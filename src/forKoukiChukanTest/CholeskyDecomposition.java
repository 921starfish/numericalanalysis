package forKoukiChukanTest;

import static util.Calc.*;

public class CholeskyDecomposition {

	public static void main(String[] args) {
		int n = 4;
		// double alpha = 0;

		// double[][] A = new double[n][n];
		// double[] b = new double[n];

		double[][] A = { { 1, -1, 2, 1 }, { -1, 5, -4, 3 }, { 2, -4, 14, -3 },
				{ 1, 3, -3, 10 } };
		double[] b = { 1, 1, 1, 1 };

		double[][] L = new double[n][n];

		double[] x = new double[b.length];
		double[] y = new double[b.length];

		// Cholesky分解パート
		for (int j = 0; j < n; j++) {
			for (int i = j; i < n; i++) {
				double s = 0;
				for (int k = 0; k < j; k++) {
					s = s + L[i][k] * L[j][k];
				}
				if (i == j) {
					L[i][i] = Math.sqrt(A[i][i] - s);
				} else {
					L[i][j] = (A[i][j] - s) / L[j][j];
				}
			}
		}
		printMat(L);

		// 代入パート
		// 前進代入
		for (int k = 0; k < y.length; k++) {
			double temp = 0;
			for (int j = 0; j < k; j++) {
				temp = temp + L[k][j] * y[j];
			}

			y[k] = (b[k] - temp) / L[k][k];
		}

		printVec(y);// 前進代入終了時点

		// 後退代入
		for (int k = x.length - 1; k >= 0; k--) {
			double temp = 0;
			for (int j = k + 1; j < x.length; j++) {
				temp = temp + L[j][k] * x[j]; // 転置Ｌのため、添字が逆にしてある
			}

			x[k] = (y[k] - temp) / L[k][k];
		}

		printVec(x);// 後退代入終了時点
	}
}
