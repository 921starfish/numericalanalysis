package forKoukiChukanTest;

import static util.Calc.*;

public class GaussianEliminationWithPivoting {

	public static void main(String[] args) {
		
		int n = 3;

		// 初期化
		// double[][] A = new double[n][n];
		// double[] b = new double[n];
		double[][] A = { { 1.0e-15, 2, 0 }, { 2, 2, 2 }, { 0, 2, 1.0e+15 } };
		
		double[] b = new double[n];

		b[0] = 2 + 1.0E-15;
		b[1] = 6;
		b[2] = 2 + 1.0E+15;


		
		gaussPivot(A, b);

	}

	public static void gaussPivot(double[][] A, double[] b) {
		double alpha = 0;

		double[] x = new double[b.length];
		
		double[] truex = { 1, 1, 1 };
		double[][] oriA = { { 1.0e-15, 2, 0 }, { 2, 2, 2 }, { 0, 2, 1.0e+15 } };
		
		double[] orib = new double[3];

		orib[0] = 2 + 1.0E-15;
		orib[1] = 6;
		orib[2] = 2 + 1.0E+15;


		// ここからアルゴリズム

		// Pivot選択付き前進消去
		for (int k = 0; k < A.length - 1; k++) {
			int l = k;

			// Step 1-1
			for (int i = k; i < A.length; i++) {
				if (Math.abs(A[i][k]) > Math.abs(A[l][k])) {
					l = i;
				}
			}
			// Step 1-2
			if (A[l][k] == 0) {
				System.out.println("例外:絶対値最大成分が0");
			}
			// Step 1-3
			for (int j = k; j < A.length; j++) {
				double temp = A[k][j];
				A[k][j] = A[l][j];
				A[l][j] = temp;
			}
			double temp = b[k];
			b[k] = b[l];
			b[l] = temp;
			// Step 1-4
			for (int i = k + 1; i < A.length; i++) {
				alpha = A[i][k] / A[k][k];
				for (int j = k + 1; j < A.length; j++) {
					A[i][j] = A[i][j] - alpha * A[k][j];
				}
				b[i] = b[i] - alpha * b[k];
			}
		}

		printMat(A);// 前進消去終了時点
		printVec(b);// 前進消去終了時点

		// 後退代入
		for (int k = x.length - 1; k >= 0; k--) {
			double temp = 0;
			for (int j = k + 1; j < x.length; j++) {
				temp = temp + A[k][j] * x[j];
			}

			x[k] = (b[k] - temp) / A[k][k];
		}

		printVec(x);// 後退代入終了時点
		
		System.out.println(vecNormInf(subVec(truex, x)) / vecNormInf(truex));
		System.out.println(vecNormInf(residual(oriA, x, orib))
				/ vecNormInf(orib));
		
		//(2) の結果より,得られた近似解 x1 に対する相対誤差 ∞-ノルムは 10^−10 程度であり, 近似解の各成分は 9
		// 桁以上正しい.しかし,(3) で右辺項に微小な誤差が混入したとき, 得られた近似解 x_2 に対する相対誤差 ∞-ノルムは 10^1
		// を上回っており,近似解成分で 一桁も正しくない成分がある.右辺項に混入した誤差 ∆b が解に与える影響は条件数に
		// 依存しており,理論的に以下の関係式が成り立っている.
		/*
		 * ∥∆x∥∞/∥x∗∥∞<κ∞(A)×∥∆b∥∞/∥b∥∞
		 */
		// ただし,∆x は解に含まれる誤差である.いま,∥∆b∥∞/∥b∥∞ = 10^−3 であるが, 条件数が 10^7
		// 程度であるため,解には大きな誤差が含まれてしまう.これにより, (3) では精度の良い近似解が得られなかったと考えられる.

	}
}
