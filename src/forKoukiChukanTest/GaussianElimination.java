package forKoukiChukanTest;

import static util.Calc.*;

public class GaussianElimination {

	public static void main(String[] args) {
		int n = 3;
		double alpha = 0;

		// 初期化
		// double[][] A = new double[n][n];
		// double[] b = new double[n];
		double[][] A = { { 1.0e-15, 2, 0 }, { 2, 2, 2 }, { 0, 2, 1.0e+15 } };
		double[][] oriA = { { 1.0e-15, 2, 0 }, { 2, 2, 2 }, { 0, 2, 1.0e+15 } };
		double[] b = new double[n];

		b[0] = 2 + 1.0E-15;
		b[1] = 6;
		b[2] = 2 + 1.0E+15;

		double[] orib = new double[n];

		orib[0] = 2 + 1.0E-15;
		orib[1] = 6;
		orib[2] = 2 + 1.0E+15;

		double[] x = new double[b.length];

		double[] truex = { 1, 1, 1 };

		// ここからアルゴリズム
		outside: for (int k = 0; k < A.length - 1; k++) {
			for (int i = k + 1; i < A.length; i++) {
				if (A[k][k] == 0) {
					System.out.println("０で割ってる");
					break outside;
				}
				alpha = A[i][k] / A[k][k];
				for (int j = k + 1; j < A.length; j++) {
					A[i][j] = A[i][j] - alpha * A[k][j];
				}
				b[i] = b[i] - alpha * b[k];
			}
		}

		printMat(A);// 前進消去終了時点
		printVec(b);// 前進消去終了時点

		for (int k = x.length - 1; k >= 0; k--) {
			double temp = 0;
			for (int j = k + 1; j < x.length; j++) {
				temp = temp + A[k][j] * x[j];
			}

			x[k] = (b[k] - temp) / A[k][k];
		}

		printVec(x);// 後退代入終了時点

		System.out.println(vecNormInf(subVec(truex, x)) / vecNormInf(truex));
		System.out.println(vecNormInf(residual(oriA, x, orib))
				/ vecNormInf(orib));

	}
}
