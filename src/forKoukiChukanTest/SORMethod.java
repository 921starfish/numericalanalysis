package forKoukiChukanTest;

import static util.Calc.*;

public class SORMethod {

	public static void main(String[] args) {
		final int a = 4;
		final int n = 100;
		double[][] A = new double[n][n];
		A[0][0] = 8;
		A[0][1] = a;
		A[1][0] = a;
		A[99][98] = a;
		A[99][99] = 8;
		for (int i = 1; i < n; i++) {
			for (int j = 1; j < n; j++) {
				if (i == j) {
					A[i][j] = 8;
				}
				if (i == j - 1) {
					A[i][j] = a;
				}
				if (i - 1 == j) {
					A[i][j] = a;
				}
			}
		}
		printMat(A);
		System.out.println("------");

		double[] b = new double[n];
		for (int i = 0; i < n; i++) {
			b[i] = 1;
		}
		double[] x_0 = new double[n];
		for (int i = 1; i <= 20; i++) {
			sORMethod(A, b, x_0, (double) i / 10);
			// printVec(sORMethod(A, b, x_0, (double) i / 10));
		}

	}

	public static double[] sORMethod(double[][] A, double[] b, double[] x_ori,
			double omega) {
		final double epsilon = 1.0E-10;
		final int Nmax = 1000;

		double[] x = new double[x_ori.length];
		double[] x_mae = new double[x_ori.length];

		// SOR法はωの値を変えながらfor文で回すので、x_oriの値は保持。
		for (int i = 0; i < x.length; i++) {
			x_mae[i] = x_ori[i];
		}

		// x_mae[]の初期値の成分が全て0ならこの部分が与える影響はない
		// テストでそうじゃない初期値が与えられた場合は影響を与えるかもなので注意
		for (int i = 0; i < x.length; i++) {
			x[i] = x_mae[i];
		}

		for (int m = 0; m < Nmax; m++) {
			for (int i = 0; i < x.length; i++) {
				double sum = 0;
				for (int j = 0; j < x.length; j++) {
					if (i != j) {
						sum += A[i][j] * x[j];
					}
				}

				x[i] = (b[i] - sum) / A[i][i];

				// この下の一行がSOR法で追加した加速／緩和部分
				x[i] = (1 - omega) * x_mae[i] + omega * x[i];
			}

			// ∞-誤差ノルムによる判定
			/*
			 * if (vecNormInf(subVec(x, x_mae)) < epsilon) {
			 * System.out.print("ω = " + omega + ", ");
			 * System.out.printf("判定法は∞-誤差ノルム,反復回数は%d回%n", m + 1); return x; }
			 */

			// ∞-残差ノルムによる判定
			/*
			 * if (vecNormInf(residual(A, x, b)) < epsilon) {
			 * System.out.print("ω = " + omega + ", ");
			 * System.out.printf("判定法は∞-残差ノルム,反復回数は%d回%n", m + 1); return x; }
			 */

			// ∞-相対誤差ノルムによる判定
			if (vecNormInf(subVec(x, x_mae)) / vecNormInf(x) < epsilon) {
				System.out.print("ω = " + omega + ", ");
				System.out.printf("判定法は∞-相対誤差ノルム,反復回数は%d回%n", m + 1);
				return x;
			}

			// ∞-相対残差ノルムによる判定
			/*
			 * if (vecNormInf(residual(A, x, b)) / vecNormInf(b) < epsilon) {
			 * System.out.print("ω = " + omega + ", ");
			 * System.out.printf("判定法は∞-相対残差ノルム,反復回数は%d回%n", m + 1); return x; }
			 */

			for (int i = 0; i < x.length; i++) {
				x_mae[i] = x[i];
			}
		}

		System.out.print("ω = " + omega + ", ");
		System.out.println("収束しない");
		return x;
	}
}
