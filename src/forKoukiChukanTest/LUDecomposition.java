package forKoukiChukanTest;

import static util.Calc.*;

public class LUDecomposition {

	public static void main(String[] args) {
		// 初期化
		double[][] A = { { -1.1, 2.2, 3.3 }, { 4.4, -5.5, 6.6 },
				{ 7.7, 8.8, -9.9 } };
		double[] b = { 7, 7, 7 };
		lUDecomposition(A, b);

	}

	public static void lUDecomposition(double[][] A, double[] b) {

		double[] x = new double[b.length];
		double[] y = new double[b.length];

		// ここからアルゴリズム
		for (int k = 0; k < A.length - 1; k++) {
			for (int i = k + 1; i < A.length; i++) {
				A[i][k] = A[i][k] / A[k][k];
				for (int j = k + 1; j < A.length; j++) {
					A[i][j] = A[i][j] - A[i][k] * A[k][j];
				}
			}
		}

		printMat(A);// 前進消去終了時点
		printVec(b);// 前進消去終了時点

		// 前進代入
		for (int k = 0; k < y.length; k++) {
			double temp = 0;
			for (int j = 0; j < k; j++) {
				temp = temp + A[k][j] * y[j];
			}

			y[k] = b[k] - temp;
		}

		// 後退代入
		for (int k = x.length - 1; k >= 0; k--) {
			double temp = 0;
			for (int j = k + 1; j < x.length; j++) {
				temp = temp + A[k][j] * x[j];
			}

			x[k] = (y[k] - temp) / A[k][k];
		}

		printVec(x);// 後退代入終了時点

	}
}
