package forKoukiChukanTest;

import static util.Calc.*;

public class LUInverse {

	public static void main(String[] args) {
		double[][] A = { { 2, -1, 2 }, { -4, 4, -5 }, { 6, -7, 10 } };
		inverse(A);
	}

	public static double[][] inverse(double[][] A) {
		// 検算用に元のAを保存
		double[][] oriA = new double[A.length][A[0].length];
		for (int i = 0; i < A.length; i++) {
			for (int j = 0; j < A[0].length; j++) {
				oriA[i][j] = A[i][j];
			}
		}

		// double[][] X = new double[A.length][A[0].length];
		// double[][] Y = new double[A.length][A[0].length];
		double[][] E = new double[A.length][A[0].length];
		for (int i = 0; i < E.length; i++) {
			for (int j = 0; j < E[0].length; j++) {
				if (i == j) {
					E[i][j] = 1;
				} else {
					E[i][j] = 0;
				}
			}
		}

		// LU分解
		for (int k = 0; k < A.length - 1; k++) {
			for (int i = k + 1; i < A.length; i++) {
				A[i][k] = A[i][k] / A[k][k];
				for (int j = k + 1; j < A.length; j++) {
					A[i][j] = A[i][j] - A[i][k] * A[k][j];
				}
			}
		}

		System.out.println("LU=");
		printMat(A);
		System.out.println();

		// A = LUと分解されているとする。
		for (int i = 0; i < A.length; i++) {
			// 前進代入によりLy_i = e_iを解く
			for (int k = 0; k < A.length; k++) {
				// E[k][i] = E[k][i];
				for (int j = 0; j < k; j++) {
					E[k][i] = E[k][i] - A[k][j] * E[j][i];
				}
			}

			// 後退代入によりUx_i = y_iを解く
			for (int k = A.length - 1; k >= 0; k--) {
				// E[k][i] = E[k][i];
				for (int j = k + 1; j < A.length; j++) {
					E[k][i] = E[k][i] - A[k][j] * E[j][i];
				}
				E[k][i] = E[k][i] / A[k][k];
			}
		}

		System.out.printf("Ainv=%n");
		printMat(E);
		System.out.printf("%nA*Ainv=%n");
		printMat(multipleMat(oriA, E));

		return (E);
	}
}
