package forKoukiChukanTest;

import static util.Calc.matNormInf;
import static util.Calc.vecNormInf;

public class DaimonFour {

	public static void main(String[] args) {
		int n = 3;

		// 与えられた行列の作成
		double[][] A = { { 1.0e-15, 2, 0 }, { 2, 2, 2 }, { 0, 2, 1.0e+15 } };

		double[][] tempA = new double[A.length][A[0].length];
		for (int i = 0; i < A.length; i++) {
			for (int j = 0; j < A[0].length; j++) {
				tempA[i][j] = A[i][j];
			}
		}

		double[][] A_inv = k7.LUInverse.inverse(tempA);

		// ここでtempAの値が書き換わってしまう。

		System.out.println("事前準備の終了");

		// (1) κ_∞(A)の導出

		double A_mugen = matNormInf(A);
		double A_inv_mugen = matNormInf(A_inv);
		System.out.println("(1)N=" + n + "について");
		System.out.println(A_mugen * A_inv_mugen);

		// (3)
		double[] b = new double[n];

		for (int i = 0; i < n; i++) {
			double sum = 0;
			for (int j = 0; j < n; j++) {
				sum += A[i][j];
			}
			b[i] = sum;
		}

		double[] deltab = new double[n];

		deltab[0] = 0.001 * b[0];

		System.out.println("(3)N=" + n + "について");
		System.out.println(A_mugen * A_inv_mugen * vecNormInf(deltab)
				/ vecNormInf(b));

	}

}
