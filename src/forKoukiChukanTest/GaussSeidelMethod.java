package forKoukiChukanTest;

import static util.Calc.*;

public class GaussSeidelMethod {

	public static void main(String[] args) {
		int a = 1;
		double[][] A = { { 8, 4, 0, 0, 0 }, { a, 8, 4, 0, 0 },
				{ 0, a, 8, 4, 0 }, { 0, 0, a, 8, 4 }, { 0, 0, 0, a, 8 } };
		double[] b = { 1, 1, 1, 1, 1 };
		double[] x_0 = { 0, 0, 0, 0, 0 };
		printVec(gaussSeidelMethod(A, b, x_0));

	}

	public static double[] gaussSeidelMethod(double[][] A, double[] b,
			double[] x_mae) {
		final double epsilon = 1.0E-10;
		final int Nmax = 1000;

		double[] x = new double[x_mae.length];

		// x_mae[]の初期値の成分が全て0ならこの部分が与える影響はない
		// テストでそうじゃない初期値が与えられた場合は影響を与えるかもなので注意
		for (int i = 0; i < x.length; i++) {
			x[i] = x_mae[i];
		}

		for (int m = 0; m < Nmax; m++) {
			for (int i = 0; i < x.length; i++) {
				double sum = 0;
				for (int j = 0; j < x.length; j++) {
					if (i != j) {
						sum += A[i][j] * x[j];
					}
				}

				x[i] = (b[i] - sum) / A[i][i];
			}

			// ∞-誤差ノルムによる判定
			/*
			 * if (vecNormInf(subVec(x, x_mae)) < epsilon) {
			 * System.out.printf("判定法は∞-誤差ノルム%n反復回数は%d回%n", m + 1); return x; }
			 */

			// ∞-残差ノルムによる判定
			/*
			 * if (vecNormInf(residual(A, x, b)) < epsilon) {
			 * System.out.printf("判定法は∞-残差ノルム%n反復回数は%d回%n", m + 1); return x; }
			 */

			// ∞-相対誤差ノルムによる判定
			if (vecNormInf(subVec(x, x_mae)) / vecNormInf(x) < epsilon) {
				System.out.printf("判定法は∞-相対誤差ノルム%n反復回数は%d回%n", m + 1);
				return x;
			}

			// ∞-相対残差ノルムによる判定
			/*
			 * if (vecNormInf(residual(A, x, b)) / vecNormInf(b) < epsilon) {
			 * System.out.printf("判定法は∞-相対残差ノルム%n反復回数は%d回%n", m + 1); return x;
			 * }
			 */

			for (int i = 0; i < x.length; i++) {
				x_mae[i] = x[i];
			}
		}

		System.out.println("収束しない");
		return null;
	}
}
