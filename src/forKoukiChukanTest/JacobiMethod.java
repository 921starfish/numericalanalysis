package forKoukiChukanTest;

import static util.Calc.*;

public class JacobiMethod {

	public static void main(String[] args) {
		double[][] A = { { 6, 1, 1, 1, 0 }, { 1, 7, 1, 1, 1 },
				{ 0, 1, 8, 1, 1 }, { 0, 0, 1, 9, 1 }, { 0, 0, 0, 1, 10 } };
		double[] b = { 9, 11, 11, 11, 11 };
		double[] x_0 = { 0, 0, 0, 0, 0 };
		printVec(jacobiMethod(A, b, x_0));

	}

	public static double[] jacobiMethod(double[][] A, double[] b, double[] x_mae) {
		final double epsilon = 1.0E-10;
		final int Nmax = 100;

		double[] x = new double[x_mae.length];

		for (int m = 0; m < Nmax; m++) {
			for (int i = 0; i < x.length; i++) {
				double sum = 0;
				for (int j = 0; j < x.length; j++) {
					if (i != j) {
						sum += A[i][j] * x_mae[j];
					}
				}

				x[i] = (b[i] - sum) / A[i][i];
			}

			// ∞-誤差ノルムによる判定
			/*
			 * if (vecNormInf(subVec(x, x_mae)) < epsilon) {
			 * System.out.printf("判定法は∞-誤差ノルム%n反復回数は%d回%n", m + 1); return x; }
			 */

			// ∞-残差ノルムによる判定
			/*
			 * if (vecNormInf(residual(A, x, b)) < epsilon) {
			 * System.out.printf("判定法は∞-残差ノルム%n反復回数は%d回%n", m + 1); return x; }
			 */

			// ∞-相対誤差ノルムによる判定
			if (vecNormInf(subVec(x, x_mae)) / vecNormInf(x) < epsilon) {
				System.out.printf("判定法は∞-相対誤差ノルム%n反復回数は%d回%n", m + 1);
				return x;
			}

			// ∞-相対残差ノルムによる判定
			/*
			 * if (vecNormInf(residual(A, x, b)) / vecNormInf(b) < epsilon) {
			 * System.out.printf("判定法は∞-相対残差ノルム%n反復回数は%d回%n", m + 1); return x;
			 * }
			 */

			for (int i = 0; i < x.length; i++) {
				x_mae[i] = x[i];
			}
		}

		System.out.println("収束しない");
		return null;
	}
}
