package util;

public class Example {

	public static void main(String[] args) {
		
		/* 2015/9/24 */
		double[] x = new double[100];
		for (int i = 0; i < 100; i++) {
			x[i] = Math.sqrt(i + 1);
		}

		System.out.println(Calc.vecNorm1(x));
		System.out.println(Calc.vecNorm2(x));
		System.out.println(Calc.vecNormInf(x));

		double[][] y = new double[100][100];
		for (int i = 0; i < 100; i++) {
			for (int j = 0; j < 100; j++) {
				y[i][j] = Math.sqrt(2.0 * (i + 1) + (j + 1));
			}
		}

		System.out.println(Calc.matNorm1(y));
		System.out.println(Calc.matNormInf(y));
	}

}
