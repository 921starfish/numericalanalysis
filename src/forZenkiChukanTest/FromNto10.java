package forZenkiChukanTest;

public class FromNto10 {

	public static void main(String[] args) {
		int[] z = { 1, 1, 1, 0, 1, 1, 0, 1, 0 };
		int[] y = { 0, 1, 1, 1 };
		// zに整数部、yに小数部を一桁ずつ代入していく。
		/*
		 * 16進数を扱うときは A = 10 B = 11 C = 12 D = 13 E = 14 F = 15 として代入すればよい。
		 */
		System.out.println(Henkan(z, y));

	}

	public static double Henkan(int[] z, int[] y) {
		int N = 2; // ここを書き換える
		int seisuubu = 0;
		double syousuubu = 0d;

		for (int k = 0; k < z.length; k++) {
			seisuubu = seisuubu + z[k] * (int) Math.pow(N, (z.length - k - 1));
		}

		for (int k = 0; k < y.length; k++) {
			syousuubu = syousuubu + y[k] * Math.pow(N, -(k + 1));
		}

		if (syousuubu >= 1) {
			System.out.print("小数部が1より大きいです。");
			return 0;
		}

		return seisuubu + syousuubu;
	}

}
