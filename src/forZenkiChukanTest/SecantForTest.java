package forZenkiChukanTest;

import java.util.Scanner;

public class SecantForTest {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.print("初期値を入力：x0 = ");
		final double x0 = scan.nextDouble();
		scan.close();
		final double x1 = x0 + 1.0;
		System.out.println("　　　　　　：x1 = " + x1);
		final double epsilon = 1E-10;// = 0.0000000001
		final int Nmax = 300;
		int k;

		// 相対誤差による収束判定
		double x = x0;
		double x_bb = x0;
		double x_b = x1;
		for (k = 1; k < Nmax; k++) {
			double temp = x;
			x = x - (f(x) * (x_b - x_bb)) / (f(x_b) - f(x_bb));
			x_bb = x_b;
			x_b = x;
			if (Math.abs((x - temp) / x) < epsilon) {
				break;
			}
		}
		System.out.print("相対誤差による収束判定の");
		System.out.println("近似解は x" + (k + 1) + "=" + x);
		System.out.println("反復回数は" + (k - 1));
		System.out.print("このとき絶対誤差は");
		System.out.printf("%.2e%n", Math.abs(0.5 - x));// ここを書き換える

		// 残差による収束判定
		x = x0;
		x_bb = x0;
		x_b = x1;
		for (k = 1; k < Nmax; k++) {
			if (Math.abs(f(x)) < epsilon) {
				break;
			}
			x = x - (f(x) * (x_b - x_bb)) / (f(x_b) - f(x_bb));
			x_bb = x_b;
			x_b = x;
		}
		System.out.print("残差による収束判定の");
		System.out.println("近似解は x" + k + "=" + x);
		System.out.println("反復回数は" + (k - 1));
		System.out.print("このとき絶対誤差は");
		System.out.printf("%.2e%n", Math.abs(0.5 - x));// ここを書き換える

		// 誤差による収束判定
		x = x0;
		x_bb = x0;
		x_b = x1;
		for (k = 1; k < Nmax; k++) {
			double temp = x;
			x = x - (f(x) * (x_b - x_bb)) / (f(x_b) - f(x_bb));
			x_bb = x_b;
			x_b = x;
			if (Math.abs((x - temp)) < epsilon) {
				break;
			}
		}
		System.out.print("誤差による収束判定の");
		System.out.println("近似解は x" + (k + 1) + "=" + x);
		System.out.println("反復回数は" + (k - 1));

	}

	public static double f(double x) {
		// ここを書き換える
		return x * x * x - 4.0 * x * x + (13.0 / 4.0) * x - 3.0 / 4.0;
	}

	public static double df(double x) {
		// ここを書き換える
		return 3.0 * x * x - 8.0 * x + 13.0 / 4.0;
	}

}
