package forZenkiChukanTest;

public class IntRadixConversion {

	public static void main(String[] args) {

		// ｎ進数表記でint値を生成する
		int val10 = 127; // 10進数「127」
		int a = Integer.parseInt("01111111", 2); // 2進数「01111111」
		int val2 = 0b01111111; // 2進数「01111111」
		int val8 = 0177; // 8進数「177」
		int val16 = 0x7f; // 16進数「7f」

		// 画面表示
		System.out.println(val10);
		System.out.println(a);
		System.out.println(val2);
		System.out.println(val8);
		System.out.println(val16);

		// int値を生成する
		int i = 127;

		// int値をｎ進数表記に変換する
		System.out.println("１０進数：" + i);
		System.out.println("２進数：" + Integer.toBinaryString(i)); // 2進数表記に変換
		System.out.println("８進数：" + Integer.toOctalString(i)); // 8進数表記に変換
		System.out.println("１６進数：" + Integer.toHexString(i)); // 16進数表記に変換

	}
}
