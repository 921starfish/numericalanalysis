package forZenkiChukanTest;

import java.util.Scanner;

public class ParallelForTest {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.print("初期値を入力：x0 = ");
		final double x0 = scan.nextDouble();
		scan.close();
		final double df_x0 = df(x0);
		final double epsilon = 1E-8;// = 0.00000001
		final int Nmax = 200;
		int k;

		// 相対誤差による収束判定
		double x = x0;
		for (k = 0; k < Nmax; k++) {
			double temp = x;
			x = x - f(x) / df_x0;
			if (Math.abs((x - temp) / x) < epsilon) {
				break;
			}
		}
		System.out.print("相対誤差による収束判定の");
		System.out.println("近似解は x" + (k + 1) + "=" + x);
		System.out.println("反復回数は" + k);
		System.out.print("このとき絶対誤差は");
		System.out.printf("%.2e%n", Math.abs(3.0 - x));// ここを書き換える

		// 残差による収束判定
		x = x0;
		for (k = 0; k < Nmax; k++) {
			if (Math.abs(f(x)) < epsilon) {
				break;
			}
			x = x - f(x) / df_x0;
		}
		System.out.print("残差による収束判定の");
		System.out.println("近似解は x" + k + "=" + x);
		System.out.println("反復回数は" + k);
		System.out.print("このとき絶対誤差は");
		System.out.printf("%.2e%n", Math.abs(3.0 - x));// ここを書き換える

		// 誤差による収束判定
		x = x0;
		for (k = 0; k < Nmax; k++) {
			double temp = x;
			x = x - f(x) / df_x0;
			if (Math.abs((x - temp)) < epsilon) {
				break;
			}
		}
		System.out.print("誤差による収束判定の");
		System.out.println("近似解は x" + (k + 1) + "=" + x);
		System.out.println("反復回数は" + k);

	}

	public static double f(double x) {
		// ここを書き換える
		return x * x * x - 2.0 * x * x - (11.0 / 4.0) * x - 3.0 / 4.0;
	}

	public static double df(double x) {
		// ここを書き換える
		return 3.0 * x * x - 4.0 * x - (11.0 / 4.0);
	}

}
