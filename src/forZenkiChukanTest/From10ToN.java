package forZenkiChukanTest;

import java.util.Scanner;

public class From10ToN {

	public static void main(String[] args) {
		System.out.println("任意の実数値を入力");
		Scanner scan = new Scanner(System.in);
		double a = scan.nextDouble();
		scan.close();
		radixConversion(a);
	}

	public static void radixConversion(double z) {
		int N = 2; // ここを書き換える 
		int counta = 0;
		int countb = 0;
		String seisuubu = "";
		String syousubu = "";

		int[] a = new int[100];
		int[] b = new int[100];
		int x = (int) z;
		double y = z - x;

		for (int i = 0; i < 100; i++) {
			a[i] = x % N;
			if (x == 0) {
				counta = i;
				break;
			}
			x = x / N;

		}

		for (int i = 0; i < 100; i++) {
			b[i] = (int) (N * y);
			if (y == 0) {
				countb = i;
				break;
			}
			y = N * y - b[i];

		}

		for (int i = counta - 1; i >= 0; i--) {
			seisuubu += a[i];//16進数を扱う場合は、seisuubu += (a[i]+"_");
		}

		for (int i = 0; i < Math.max(1, countb); i++) {
			syousubu += b[i]; //16進数を扱う場合は、syousubu += (b[i]+"_");
		}

		System.out.println("与えられた実数 z = (" + z + ")_10");
		System.out.println("z の整数部分 x = " + (int) z);
		System.out.println("z の少数部分 y = " + (z - (int) z));
		System.out.println(N + "進数へ変換後の値:(" + seisuubu + "." + syousubu + ")_"
				+ N);
		@SuppressWarnings("unused")
		double temp = Double.valueOf(seisuubu + "." + syousubu);//16進数を扱う場合は、ここを代わりにdouble temp = 0;にしないとエラる
		return;

	}
}
