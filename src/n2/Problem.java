package n2;

import static java.lang.System.*;

public class Problem {

	public static void main(String[] args) {
		
		out.println(kaijou(5));
		out.println(factorial(5));
		// 1-(1)
		out.println("問題1-(1)");

		double a = Math.pow(4d, 7d);
		double b = Math.pow(7d, 9d);
		double c = Math.sqrt(a / b);
		out.println(c);
		out.println();

		// 1-(2)
		out.println("問題1-(2)");

		a = Math.cos((3d / 4) * Math.PI);
		b = Math.pow(Math.E, 6d) * Math.sqrt(Math.log(5d));
		c = Math.abs(a - b);
		out.println(c);
		out.println();

		// 2
		out.println("問題2");

		out.println(Fibonacci(40));
		out.println();

		// 3
		out.println("問題3");

		for (int i = 1; true; i++) {
			if (suretsu3(i)[0] < 1.0E-5) {
				out.println(i);// n
				out.println(suretsu3(i)[1]);// s
				break;
			}
		}
		out.println();

		// 4
		out.println("問題4");

		double sum = 0;
		for (int i = 0; i <= 10; i++) {
			sum += 1d / (double) kaijou(i);
		}
		out.println(sum);
		out.println();

		// 5
		out.println("問題5");

		double[][] A = new double[3][3];
		double[][] B = new double[3][3];
		double[][] C = new double[3][3];

		// 初期化
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				A[i][j] = (i + 1) + (j + 1);
				B[i][j] = Math.sqrt((i + 1) * (j + 1));
			}
		}

		// 演算
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				C[i][j] = A[i][j] + B[i][j];
			}
		}

		// 出力
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				out.println("C[" + i + "][" + j + "]=" + C[i][j]);
			}
		}

	}

	// 以下static method
	// 2
	static int Fibonacci(int a) {
		if (a < 0) {
			return 0;
		}

		if (a == 0) {
			return 0;
		} else if (a == 1) {
			return 1;
		} else {
			return Fibonacci(a - 1) + Fibonacci(a - 2);
		}
	}

	// 3
	static double[] suretsu3(int n) {
		double[] data = { 1d, 0d };
		for (int i = 0; i < n; i++) {
			data[0] *= 1d / 3;// 数列
			data[1] += data[0];// 総和
		}
		return data;
	}

	// 4
	static int kaijou(int n) {
		if (n == 0) {
			return 1;
		} else {
			return n * kaijou(n - 1);
		}
	}

	static int factorial(int n) {
		if (n == 0) {
			return 1;
		} else {
			for (int i = n - 1; i > 0; i--) {
				n *= i;
			}
			return n;
		}
	}
}
