// f(x)=x * x * x + x * x - 5.0 * x + 3.0;
package n10;

import java.util.Scanner;

public class NewtonMethod {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.print("初期値を入力：x0 = ");
		final double x0 = scan.nextDouble();
		scan.close();
		final double epsilon = 1E-12;// = 0.0000000001
		final int Nmax = 50;
		int k;

		// 残差による収束判定
		double x = x0;
		for (k = 0; k < Nmax; k++) {
			System.out.printf("|e_%d|=%.2e%n", k, Math.abs(-3.0 - x));// 真の解は毎回書き換える
			if (Math.abs(f(x)) < epsilon) {
				break;
			}
			x = x - f(x) / df(x);
		}
		System.out.print("残差による収束判定の");
		System.out.println("近似解は x" + k + "=" + x);
		System.out.print("このとき絶対誤差は");
		System.out.printf("%.2e%n", Math.abs(-3.0 - x)); // 真の解は毎回書き換える

	}

	public static double f(double x) {
		return x * x * x + x * x - 5.0 * x + 3.0;
	}

	public static double df(double x) {
		return 3.0 * x * x + 2.0 * x - 5.0;
	}

}
