package hilbertForKouki;

public class HilbertTest2and4 {

	public static void main(String[] args) {
		int n = 6;

		// Hilbert行列の作成
		double[][] A = new double[n][n];

		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				A[i][j] = 1.0 / ((i + 1) + (j + 1) - 1);
			}
		}

		double[][] tempA = new double[A.length][A[0].length];
		for (int i = 0; i < A.length; i++) {
			for (int j = 0; j < A[0].length; j++) {
				tempA[i][j] = A[i][j];
			}
		}

		System.out.println("事前準備の終了");
		// (2) Ax=b
		double[] b = new double[n];

		for (int i = 0; i < n; i++) {
			double sum = 0;
			for (int j = 0; j < n; j++) {
				sum += A[i][j];
			}
			b[i] = sum;
		}

		System.out.println("(2)N=" + n + "について残差ノルムは");
		System.out.println(GaussWithPivotForEnsyu.gaussPivot(tempA, b));

		// 再度初期化
		// A
		for (int i = 0; i < A.length; i++) {
			for (int j = 0; j < A[0].length; j++) {
				tempA[i][j] = A[i][j];
			}
		}

		// b
		for (int i = 0; i < n; i++) {
			double sum = 0;
			for (int j = 0; j < n; j++) {
				sum += A[i][j];
			}
			b[i] = sum;
		}

		double[] x = new double[n];

		for (int i = 0; i < x.length; i++) {
			x[i] = 1;
		}

		System.out.println("(2)N=" + n + "について誤差ノルムは");
		System.out.println(GaussWithPivotForEnsyu.gaussPivot(tempA, b, x));

		// (4)
		// 再度初期化
		for (int i = 0; i < A.length; i++) {
			for (int j = 0; j < A[0].length; j++) {
				tempA[i][j] = A[i][j];
			}
		}

		// b
		for (int i = 0; i < n; i++) {
			double sum = 0;
			for (int j = 0; j < n; j++) {
				sum += A[i][j];
			}
			b[i] = sum;
		}

		// x
		for (int i = 0; i < x.length; i++) {
			x[i] = 1;
		}

		// bに誤差を混入させる。
		b[0] += 0.001 * b[0];

		System.out.println("(4)N=" + n + "について誤差ノルムは");
		System.out.println(GaussWithPivotForEnsyu.gaussPivot(tempA, b, x));

	}

}
