package hilbertForKouki;

import static util.Calc.*;
import static k8.GaussWithPivotForEnsyu.gaussPivot;

public class HilbertTest5 {

	public static void main(String[] args) {
		int n = 6;

		// Hilbert行列の作成
		double[][] A = new double[n][n];

		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				A[i][j] = 1.0 / ((i + 1) + (j + 1) - 1);
			}
		}

		double[][] tempA = new double[A.length][A[0].length];
		for (int i = 0; i < A.length; i++) {
			for (int j = 0; j < A[0].length; j++) {
				tempA[i][j] = A[i][j];
			}
		}

		// bとそのコピーの用意
		double[] b = new double[n];

		double[] tempb = new double[n];

		for (int i = 0; i < n; i++) {
			double sum = 0;
			for (int j = 0; j < n; j++) {
				sum += A[i][j];
			}
			b[i] = sum;
			tempb[i] = sum;
		}

		// A_invの用意
		double[][] A_inv = k7.LUInverse.inverse(tempA);

		// ここでtempAの値が書き換わってしまう。

		System.out.println("事前準備の終了");

		// (1) κ_∞(A)の導出

		double kappa = matNormInf(A) * matNormInf(A_inv);

		// tempAの初期化
		for (int i = 0; i < A.length; i++) {
			for (int j = 0; j < A[0].length; j++) {
				tempA[i][j] = A[i][j];
			}
		}

		System.out.println("(5)N=" + n + "について");
		System.out.println(kappa * gaussPivot(tempA, tempb) / vecNormInf(b));

	}

}
