package hilbertForKouki;

import static util.Calc.*;

//演習課題4用に引数の数によって残差ノルムや誤差ノルムを返すようになってるPivot選択付ガウスの消去法
public class GaussWithPivotForEnsyu {

	public static double gaussPivot(double[][] A, double[] b) {
		double alpha = 0;
		double[] x = new double[b.length];

		double[][] oriA = new double[A.length][A[0].length];
		for (int i = 0; i < A.length; i++) {
			for (int j = 0; j < A[0].length; j++) {
				oriA[i][j] = A[i][j];
			}
		}

		double[] orib = new double[b.length];
		for (int i = 0; i < b.length; i++) {
			orib[i] = b[i];
		}

		// ここからアルゴリズム

		// Pivot選択付き前進消去
		for (int k = 0; k < A.length - 1; k++) {
			int l = k;

			// Step 1-1
			for (int i = k; i < A.length; i++) {
				if (Math.abs(A[i][k]) > Math.abs(A[l][k])) {
					l = i;
				}
			}
			// Step 1-2
			if (A[l][k] == 0) {
				System.out.println("例外:絶対値最大成分が0");
			}
			// Step 1-3
			for (int j = k; j < A.length; j++) {
				double temp = A[k][j];
				A[k][j] = A[l][j];
				A[l][j] = temp;
			}
			double temp = b[k];
			b[k] = b[l];
			b[l] = temp;
			// Step 1-4
			for (int i = k + 1; i < A.length; i++) {
				alpha = A[i][k] / A[k][k];
				for (int j = k + 1; j < A.length; j++) {
					A[i][j] = A[i][j] - alpha * A[k][j];
				}
				b[i] = b[i] - alpha * b[k];
			}
		}

		// 後退代入
		for (int k = x.length - 1; k >= 0; k--) {
			double temp = 0;
			for (int j = k + 1; j < x.length; j++) {
				temp = temp + A[k][j] * x[j];
			}

			x[k] = (b[k] - temp) / A[k][k];
		}

		return vecNormInf(residual(oriA, x, orib))/vecNormInf(orib);

	}

	public static double gaussPivot(double[][] A, double[] b, double[] truex) {
		double alpha = 0;
		double[] x = new double[b.length];

		double[][] oriA = new double[A.length][A[0].length];
		for (int i = 0; i < A.length; i++) {
			for (int j = 0; j < A[0].length; j++) {
				oriA[i][j] = A[i][j];
			}
		}

		double[] orib = new double[b.length];
		for (int i = 0; i < b.length; i++) {
			orib[i] = b[i];
		}

		// ここからアルゴリズム

		// Pivot選択付き前進消去
		for (int k = 0; k < A.length - 1; k++) {
			int l = k;

			// Step 1-1
			for (int i = k; i < A.length; i++) {
				if (Math.abs(A[i][k]) > Math.abs(A[l][k])) {
					l = i;
				}
			}
			// Step 1-2
			if (A[l][k] == 0) {
				System.out.println("例外:絶対値最大成分が0");
			}
			// Step 1-3
			for (int j = k; j < A.length; j++) {
				double temp = A[k][j];
				A[k][j] = A[l][j];
				A[l][j] = temp;
			}
			double temp = b[k];
			b[k] = b[l];
			b[l] = temp;
			// Step 1-4
			for (int i = k + 1; i < A.length; i++) {
				alpha = A[i][k] / A[k][k];
				for (int j = k + 1; j < A.length; j++) {
					A[i][j] = A[i][j] - alpha * A[k][j];
				}
				b[i] = b[i] - alpha * b[k];
			}
		}

		// 後退代入
		for (int k = x.length - 1; k >= 0; k--) {
			double temp = 0;
			for (int j = k + 1; j < x.length; j++) {
				temp = temp + A[k][j] * x[j];
			}

			x[k] = (b[k] - temp) / A[k][k];
		}

		return vecNormInf(subVec(x, truex)) / vecNormInf(truex);

	}
}
