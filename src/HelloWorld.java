//早川のSecant法
public class HelloWorld {
	static double f(double x) {
		return x * x * x + x * x - 5 * x + 3.0;
	}

	static double df(double x) {
		return 3.0 * x * x + 2.0 * x - 5.0;
	}

	public static void main(String[] args) {
		final double x0 = 30;
		final double x1 = -30;
		final double epsilon = 1E-12;
		final int Nmax = 50;
		double x;
		int i;
		double a = 1.0;
		double b = -3.0;
		// 残差による収束判定
		x = x0;
		for (i = 0; i <= Nmax; i++) {
			if (Math.abs(f(x)) < epsilon) {
				break;
			}
			x = x - f(x) / df(x);
			System.out.println("初期値" + x0 + "のNewton法による絶対誤差の値は");
			System.out.println("近似解はx" + (i + 1) + "の時" + Math.abs(a - x));
			System.out.printf("%.2e%n", Math.abs(a - x));
			System.out.println();
		}
		// 残差による収束判定
		x = x1;
		for (i = 0; i <= Nmax; i++) {
			if (Math.abs(f(x)) < epsilon) {
				break;
			}
			x = x - f(x) / df(x);
			System.out.println("初期値" + x1 + "のNewton法による絶対誤差による値は");
			System.out.println("近似解はx" + (i + 1) + "の時" + Math.abs(b - x));
			System.out.printf("%2e%n", Math.abs(b - x));
			System.out.println();
		}
	}
}