package k2;

import static util.Calc.*;

@SuppressWarnings("unused")
public class GaussianElimination {

	public static void main(String[] args) {
		int n = 500;
		double alpha = 0;

		// 初期化
		// double[][] A = new double[n][n];
		// double[] b = new double[n];
		double[][] A = { { 1, 2, 1, 2, 1 }, { 2, 3, 2, 3, 2 },
				{ 1, 2, 3, 4, 5 }, { 4, 3, 8, 1, 2 }, { 8, 2, 4, 1, 9 } };
		double[] b = { 7, 7, 7, 7, 7 };

		double[] x = new double[b.length];

		// ここからアルゴリズム
		outside: for (int k = 0; k < A.length - 1; k++) {
			for (int i = k + 1; i < A.length; i++) {
				if (A[k][k] == 0) {
					System.out.println("０で割ってる");
					break outside;
				}
				alpha = A[i][k] / A[k][k];
				for (int j = k + 1; j < A.length; j++) {
					A[i][j] = A[i][j] - alpha * A[k][j];
				}
				b[i] = b[i] - alpha * b[k];
			}
		}

		printMat(A);// 前進消去終了時点
		printVec(b);// 前進消去終了時点

		for (int k = x.length - 1; k >= 0; k--) {
			double temp = 0;
			for (int j = k + 1; j < x.length; j++) {
				temp = temp + A[k][j] * x[j];
			}

			x[k] = (b[k] - temp) / A[k][k];
		}

		printVec(x);// 後退代入終了時点

	}
}
