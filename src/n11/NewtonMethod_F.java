// f(x)=sin(x)/(x-1)
package n11;

import java.util.Scanner;

public class NewtonMethod_F {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.print("初期値を入力：x0 = ");
		final double x0 = scan.nextDouble();
		scan.close();
		final double epsilon = 1E-10;
		final int Nmax = 50;
		int k;

		// 相対誤差による収束判定
		double x = x0;
		for (k = 0; k < Nmax; k++) {
			System.out.printf("|e_%d|=%.2e%n", k, Math.abs(2*Math.PI - x));// 真の解は毎回書き換える
			double temp = x;
			x = x - f(x) / df(x);
			if (Math.abs((x - temp) / x) < epsilon) {
				break;
			}
		}
		System.out.print("相対誤差による収束判定の");
		System.out.println("近似解は x" + (k + 1) + "=" + x);
		System.out.print("このとき絶対誤差は");
		System.out.printf("|e_%d|=%.2e%n",(k+1),Math.abs(2*Math.PI -x));// 真の解は毎回書き換える

	}

	public static double f(double x) {
		return Math.sin(x)/(x-1);
	}

	public static double df(double x) {
		return (Math.cos(x)*(x-1)-Math.sin(x))/((x-1)*(x-1));
	}

}
