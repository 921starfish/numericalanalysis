// g(x)=Math.pow((x-1),4.0);
package n11;

import java.util.Scanner;

public class ParallelChordMethod_G {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.print("初期値を入力：x0 = ");
		final double x0 = scan.nextDouble();
		scan.close();
		final double dg_x0 = dg(x0);
		final double epsilon = 1E-8;
		final int Nmax = 200;
		int k;

		// 相対誤差による収束判定
		double x = x0;
		for (k = 0; k < Nmax; k++) {
			double temp = x;
			x = x - g(x) / dg_x0;
			if (Math.abs((x - temp) / x) < epsilon) {
				break;
			}
		}
		System.out.print("相対誤差による収束判定の");
		System.out.println("近似解は x" + (k + 1) + "=" + x);
		System.out.print("このとき絶対誤差は");
		System.out.printf("%.2e%n", Math.abs(3.0 - x));// 真の解は毎回書き換える

		// 残差による収束判定
		x = x0;
		for (k = 0; k < Nmax; k++) {
			if (Math.abs(g(x)) < epsilon) {
				break;
			}
			x = x - g(x) / dg_x0;
		}
		System.out.print("残差による収束判定の");
		System.out.println("近似解は x" + k + "=" + x);
		System.out.print("このとき絶対誤差は");
		System.out.printf("%.2e%n", Math.abs(3.0 - x));// 真の解は毎回書き換える

		// 誤差による収束判定
		x = x0;
		for (k = 0; k < Nmax; k++) {
			double temp = x;
			x = x - g(x) / dg_x0;
			if (Math.abs((x - temp)) < epsilon) {
				break;
			}
		}
		System.out.print("誤差による収束判定の");
		System.out.println("近似解は x" + (k + 1) + "=" + x);

	}

	public static double g(double x) {
		return (x - 1) * (x - 1) * (x - 1) * (x - 1);
	}

	public static double dg(double x) {
		return 4.0 * (x - 1) * (x - 1) * (x - 1);
	}

}
