package n5;

public class Problem {

	public static void main(String[] args) {
		// 問題１
		double a = 0.1;
		double b = 0.3;
		double c = 0.4;
		System.out.println((a + b) + c);
		System.out.println(a + (b + c));
		System.out.println();

		// 問題２
		double x = 1.0e-7;
		double fx = (1.0 - Math.cos(x)) / (x * x);
		System.out.println(fx);
		fx = (Math.sin(x) * Math.sin(x)) / (x * x + x * x * Math.cos(x));
		System.out.println(fx);
		System.out.println();

		// 問題３-１
		float sum = 0.0f;
		for (int k = 1; k <= 500; k++) {
			sum += 1.0f / (k * k);
		}
		System.out.printf("sum = %.16f%n", sum);

		sum = 0.0f;
		for (int k = 1; k <= 5000; k++) {
			sum += 1.0f / (k * k);
		}
		System.out.printf("sum = %.16f%n", sum);

		sum = 0.0f;
		for (int k = 1; k <= 50000; k++) {
			sum += 1.0f / (k * k);
		}
		System.out.printf("sum = %.16f%n", sum);

		// 問題３-２
		sum = 0.0f;
		for (int k = 1; k <= 50000; k++) {
			float sum2 = sum + 1.0f / (k * k);
			if (sum == sum2) {
				System.out.println("N = " + (k - 1));
				break;
			}
			sum = sum2;
		}

		// 問題３-３
		double sum2 = 0.0;
		for (int k = 500; k >= 1; k--) {
			sum2 += 1.0 / (k * k);
		}
		System.out.println("sum2 = " + sum2);

		sum2 = 0.0;
		for (int k = 5000; k >= 1; k--) {
			sum2 += 1.0 / (k * k);
		}
		System.out.println("sum2 = " + sum2);

		sum2 = 0.0f;
		for (int k = 50000; k >= 1; k--) {
			sum2 += 1.0 / (k * k);
		}
		System.out.println("sum2 = " + sum2);

	}

}
