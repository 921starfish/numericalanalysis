package n5;

public class Error_test {

	public static void main(String[] args) {

		float a = 0.1f;
		System.out.printf("a = %.16f%n", a);

		a = 0.5f;
		System.out.printf("a = %.16f%n", a);

		double b = 0.1 + 0.2;
		System.out.println(b);

		double val = 0.0;
		for (int i = 1; i <= 10000; i++) {
			val = val + 0.3;
		}
		System.out.println(val);
	}

}
