package k5;

import static util.Calc.*;

public class GaussianEliminationWithPivoting {

	public static void main(String[] args) {
		
		double[][] A = { { 1, 2, 1, 2, 1 }, { 2, 3, 2, 3, 2 },
				{ 1, 2, 3, 4, 5 }, { 4, 3, 8, 1, 2 }, { 8, 2, 4, 1, 9 } };
		double[] b = { 7, 7, 7, 7, 7 };
		gaussPivot(A, b);

	}

	public static void gaussPivot(double[][] A, double[] b) {
		double alpha = 0;

		double[] x = new double[b.length];

		// ここからアルゴリズム

		// Pivot選択付き前進消去
		for (int k = 0; k < A.length - 1; k++) {
			int l = k;

			// Step 1-1
			for (int i = k; i < A.length; i++) {
				if (Math.abs(A[i][k]) > Math.abs(A[l][k])) {
					l = i;
				}
			}
			// Step 1-2
			if (A[l][k] == 0) {
				System.out.println("例外:絶対値最大成分が0");
			}
			// Step 1-3
			for (int j = k; j < A.length; j++) {
				double temp = A[k][j];
				A[k][j] = A[l][j];
				A[l][j] = temp;
			}
			double temp = b[k];
			b[k] = b[l];
			b[l] = temp;
			// Step 1-4
			for (int i = k + 1; i < A.length; i++) {
				alpha = A[i][k] / A[k][k];
				for (int j = k + 1; j < A.length; j++) {
					A[i][j] = A[i][j] - alpha * A[k][j];
				}
				b[i] = b[i] - alpha * b[k];
			}
		}

		printMat(A);// 前進消去終了時点
		printVec(b);// 前進消去終了時点

		// 後退代入
		for (int k = x.length - 1; k >= 0; k--) {
			double temp = 0;
			for (int j = k + 1; j < x.length; j++) {
				temp = temp + A[k][j] * x[j];
			}

			x[k] = (b[k] - temp) / A[k][k];
		}

		printVec(x);// 後退代入終了時点

	}
}
