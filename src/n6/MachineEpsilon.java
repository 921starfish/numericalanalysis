package n6;

public class MachineEpsilon {

	public static void main(String[] args) {

		double epsilon2 = 1.0;
		while (1.0 + epsilon2 != 1.0) {
			epsilon2 /= 2;
		}
		double result2 = epsilon2 * 2;
		System.out.println(result2);

		float epsilon = 1.0f;
		while (1.0f + epsilon != 1.0f) {
			epsilon /= 2;
		}
		float result = epsilon * 2;
		// System.out.printf("%.16E%n", result);
		// System.out.println((double) result);
		System.out.println(result);

	}

}
