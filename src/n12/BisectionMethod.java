package n12;

import java.util.Scanner;

public class BisectionMethod {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.print("初期値を入力：a = ");
		double a = scan.nextDouble();
		System.out.print("初期値を入力：b = ");
		double b = scan.nextDouble();
		scan.close();
		double c = b;
		final double epsilon = 1E-12;// = 0.0000000001
		// final int Nmax = 300;
		int k;

		// 理論上の反復回数計算用
		double a0 = a;
		double b0 = b;

		if (f(a) * f(b) >= 0) {
			System.out.println("初期値が不適切です。");
			return;
		}
		for (k = 0; !(Math.abs(b - a) / 2 < epsilon); k++) {
			c = (a + b) / 2;
			if (f(a) * f(c) > 0) {
				a = c;
			} else if (f(a) * f(c) < 0) {
				b = c;
			} else {
				break;
			}
		}

		System.out.println("近似解は " + c);
		System.out.println("反復回数は " + k + " 回");
		System.out.println("|f(c)|は " + Math.abs(f(c)));

		System.out.print("理論上の反復回数は ");
		double n = 0;
		for (n = 0; !(Math.abs(b0 - a0) / Math.pow(2.0, n + 1) < 1E-12); n++) {
		}
		System.out.println(n);
	}

	public static double f(double x) {
		return x * x * x - 2 * x * x - x + 2;
	}

}
