package n3;

import java.util.Scanner;

public class From10To2 {

	public static void main(String[] args) {
		System.out.println("任意の実数値を入力");
		Scanner scan = new Scanner(System.in);
		double a = scan.nextDouble();
		scan.close();
		double b = radixConversion(a);
		System.out.println(b);
	}

	public static double radixConversion(double z) {
		int N = 2;
		int counta = 0;
		int countb = 0;
		String seisuubu = "";
		String syousubu = "";

		int[] a = new int[100];
		int[] b = new int[100];
		int x = (int) z;
		double y = z - x;

		for (int i = 0; i < 100; i++) {
			a[i] = x % N;
			if (x == 0) {
				counta = i;
				break;
			}
			x = x / N;

		}

		for (int i = 0; i < 100; i++) {
			b[i] = (int) (N * y);
			if (y == 0) {
				countb = i;
				break;
			}
			y = N * y - b[i];

		}

		for (int i = counta - 1; i >= 0; i--) {
			seisuubu += a[i];
		}

		for (int i = 0; i < Math.max(1, countb); i++) {
			syousubu += b[i];
		}

		System.out.println("与えられた実数 z = (" + z + ")_10");
		System.out.println("z の整数部分 x = " + (int) z);
		System.out.println("z の少数部分 y = " + (z - (int) z));
		System.out.println("2進数へ変換後の値:(" + seisuubu + "." + syousubu + ")_2");
		double temp = Double.valueOf(seisuubu + "." + syousubu);
		return temp;

	}
}
