package ensyuKadai2;

import static util.Calc.*;

import java.io.*;

public class GaussianEliminationTest {
	public static void main(String[] args) {
		for (int i = 0; i < 100; i++) {
			algorithm();
		}
	}

	public static void algorithm() {
		// ファイル入出力関連
		String FS = File.separator;
		String crlf = System.getProperty("line.separator");
		FileWriter fw = null;
		try {
			fw = new FileWriter(FS + "Users" + FS + "hoshino" + FS + "Desktop"
					+ FS + "data.csv", true);

			System.out.println("ピボット選択の有無による精度テスト");
			final int n = 500;
			double alpha = 0;
			double[][] sourceA = new double[n][n];
			double[] sourceb = new double[n];

			double[][] A = new double[n][n];
			double[] b = new double[n];

			// データ準備
			for (int i = 0; i < n; i++) {
				for (int j = 0; j < n; j++) {
					sourceA[i][j] = Math.random();
				}
			}

			for (int i = 0; i < n; i++) {
				sourceb[i] = Math.random();
			}

			// ここからピボット選択なしのアルゴリズム

			// データのコピー
			for (int i = 0; i < n; i++) {
				for (int j = 0; j < n; j++) {
					A[i][j] = sourceA[i][j];
				}
			}

			for (int i = 0; i < n; i++) {
				b[i] = sourceb[i];
			}

			// 前進消去
			for (int k = 0; k < A.length - 1; k++) {
				for (int i = k + 1; i < A.length; i++) {
					if (A[k][k] == 0) {
						System.out.println("０で割ってる");
					}
					alpha = A[i][k] / A[k][k];
					for (int j = k + 1; j < A.length; j++) {
						A[i][j] = A[i][j] - alpha * A[k][j];
					}
					b[i] = b[i] - alpha * b[k];
				}
			}

			// 後退代入 (近似解は配列bに上書き)
			for (int k = b.length - 1; k >= 0; k--) {
				for (int j = k + 1; j < b.length; j++) {
					b[k] = b[k] - A[k][j] * b[j];
				}

				b[k] = b[k] / A[k][k];
			}

			// 出力
			printVec(b);
			double result1 = vecNorm2(residual(sourceA, b, sourceb));
			System.out.println(result1);
			fw.write(String.valueOf(result1) + ",");

			// ここからピボット選択付きのアルゴリズム

			// データのコピー
			for (int i = 0; i < n; i++) {
				for (int j = 0; j < n; j++) {
					A[i][j] = sourceA[i][j];
				}
			}

			for (int i = 0; i < n; i++) {
				b[i] = sourceb[i];
			}

			// Pivot選択付き前進消去
			for (int k = 0; k < A.length - 1; k++) {
				int l = k;

				// Step 1-1
				for (int i = k; i < A.length; i++) {
					if (Math.abs(A[i][k]) > Math.abs(A[l][k])) {
						l = i;
					}
				}
				// Step 1-2
				if (A[l][k] == 0) {
					System.out.println("例外:絶対値最大成分が0");
				}
				// Step 1-3
				for (int j = k; j < A.length; j++) {
					double temp = A[k][j];
					A[k][j] = A[l][j];
					A[l][j] = temp;
				}
				double temp = b[k];
				b[k] = b[l];
				b[l] = temp;
				// Step 1-4
				for (int i = k + 1; i < A.length; i++) {
					alpha = A[i][k] / A[k][k];
					for (int j = k + 1; j < A.length; j++) {
						A[i][j] = A[i][j] - alpha * A[k][j];
					}
					b[i] = b[i] - alpha * b[k];
				}
			}

			// 後退代入 (近似解は配列bに上書き)
			for (int k = b.length - 1; k >= 0; k--) {
				for (int j = k + 1; j < b.length; j++) {
					b[k] = b[k] - A[k][j] * b[j];
				}

				b[k] = b[k] / A[k][k];
			}

			// 出力
			printVec(b);
			double result2 = vecNorm2(residual(sourceA, b, sourceb));
			System.out.println(result2);
			fw.write(String.valueOf(result2) + crlf);
			fw.flush();
			fw.close();
		} catch (IOException e) {
			System.out.println("ファイル書き込みエラーです");
		} finally {
			if (fw != null) {
				try {
					if (fw != null) {
						fw.close();
					}
				} catch (IOException e2) {
				}
			}
		}
	}
}
