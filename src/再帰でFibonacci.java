public class 再帰でFibonacci {

	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		System.out.println(Fibonacci(40));
		long end = System.currentTimeMillis();
		System.out.println((end - start)/1000.0 + "ms");
	}

	public static long Fibonacci(int n) {
		if (n == 0) {
			return 0;
		} else if (n == 1) {
			return 1;
		} else {
			return Fibonacci(n - 1) + Fibonacci(n - 2);
		}
	}

}
