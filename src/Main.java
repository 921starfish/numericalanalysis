import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;

public class Main {
	public static void main(String[] args) throws Exception {
		// 自分の得意な言語で
		// Let's チャレンジ！！
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String[] str = br.readLine().split(" ");
		int x = Integer.parseInt(str[0]);
		int y = Integer.parseInt(str[1]);
		int z = Integer.parseInt(str[2]);
		int n = Integer.parseInt(str[3]);
		String[][] data = new String[n][2];
		for (int i = 0; i < n; i++) {
			data[i] = br.readLine().split(" ");
		}
		br.close();

		int countx = 0;
		int county = 0;

		for (int i = 0; i < n; i++) {
			switch (data[i][0]) {
			case "0":
				countx++;
				break;
			case "1":
				county++;
				break;
			}
		}

		int[] x_array = new int[countx];
		int[] y_array = new int[county];

		int a = 0;
		int b = 0;
		for (int i = 0; i < n; i++) {
			switch (data[i][0]) {
			case "0":
				x_array[a] = Integer.parseInt(data[i][1]);
				a++;
				break;
			case "1":
				y_array[b] = Integer.parseInt(data[i][1]);
				b++;
				break;
			}
		}

		Arrays.sort(x_array);
		Arrays.sort(y_array);

		int minimumx = x_array.length == 0 ? x : x_array[0];
		int minimumy = y_array.length == 0 ? y : y_array[0];

		for (int i = 0; i < countx - 1; i++) {
			minimumx = Math.min(minimumx, x_array[i + 1] - x_array[i]);
		}
		if (x_array.length != 0) {
			minimumx = Math.min(minimumx, x - x_array[countx - 1]);
		}
		for (int i = 0; i < county - 1; i++) {
			minimumy = Math.min(minimumy, y_array[i + 1] - y_array[i]);
		}
		if (y_array.length != 0) {
			minimumy = Math.min(minimumy, y - y_array[county - 1]);
		}

		System.out.println(minimumx * minimumy * z);

	}
}
