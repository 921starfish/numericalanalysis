package k8;

import static util.Calc.*;

public class HilbertTest1and3 {

	public static void main(String[] args) {
		// 演習課題4

		// Hilbert行列の作成
		double[][] A5 = new double[5][5];
		double[][] A10 = new double[10][10];
		double[][] A15 = new double[15][15];

		for (int i = 0; i < 5; i++) {
			for (int j = 0; j < 5; j++) {
				A5[i][j] = 1.0 / ((i + 1) + (j + 1) - 1);
			}
		}
		for (int i = 0; i < 10; i++) {
			for (int j = 0; j < 10; j++) {
				A10[i][j] = 1.0 / ((i + 1) + (j + 1) - 1);
			}
		}
		for (int i = 0; i < 15; i++) {
			for (int j = 0; j < 15; j++) {
				A15[i][j] = 1.0 / ((i + 1) + (j + 1) - 1);
			}
		}

		double[][] tempA5 = new double[A5.length][A5[0].length];
		for (int i = 0; i < A5.length; i++) {
			for (int j = 0; j < A5[0].length; j++) {
				tempA5[i][j] = A5[i][j];
			}
		}
		double[][] tempA10 = new double[A10.length][A10[0].length];
		for (int i = 0; i < A10.length; i++) {
			for (int j = 0; j < A10[0].length; j++) {
				tempA10[i][j] = A10[i][j];
			}
		}
		double[][] tempA15 = new double[A15.length][A15[0].length];
		for (int i = 0; i < A15.length; i++) {
			for (int j = 0; j < A15[0].length; j++) {
				tempA15[i][j] = A15[i][j];
			}
		}

		double[][] A5_inv = k7.LUInverse.inverse(tempA5);
		double[][] A10_inv = k7.LUInverse.inverse(tempA10);
		double[][] A15_inv = k7.LUInverse.inverse(tempA15);

		// ここでtempA5,tempA10,tempA15の値が書き換わってしまう。

		System.out.println("事前準備の終了");

		// (1) κ_∞(A)の導出

		double A5_mugen = matNormInf(A5);
		double A5_inv_mugen = matNormInf(A5_inv);
		System.out.println("(1)N=5について");
		System.out.println(A5_mugen * A5_inv_mugen);

		double A10_mugen = matNormInf(A10);
		double A10_inv_mugen = matNormInf(A10_inv);
		System.out.println("(1)N=10について");
		System.out.println(A10_mugen * A10_inv_mugen);

		double A15_mugen = matNormInf(A15);
		double A15_inv_mugen = matNormInf(A15_inv);
		System.out.println("(1)N=15について");
		System.out.println(A15_mugen * A15_inv_mugen);

		// (3)
		double[] b5 = new double[5];
		double[] b10 = new double[10];
		double[] b15 = new double[15];

		for (int i = 0; i < 5; i++) {
			double sum = 0;
			for (int j = 0; j < 5; j++) {
				sum += A5[i][j];
			}
			b5[i] = sum;
		}

		for (int i = 0; i < 10; i++) {
			double sum = 0;
			for (int j = 0; j < 10; j++) {
				sum += A10[i][j];
			}
			b10[i] = sum;
		}
		for (int i = 0; i < 15; i++) {
			double sum = 0;
			for (int j = 0; j < 15; j++) {
				sum += A15[i][j];
			}
			b15[i] = sum;
		}

		double[] deltab5 = new double[5];
		double[] deltab10 = new double[10];
		double[] deltab15 = new double[15];

		deltab5[0] = 0.001 * b5[0];
		deltab10[0] = 0.001 * b10[0];
		deltab15[0] = 0.001 * b15[0];

		System.out.println("(3)N=5について");
		System.out.println(A5_mugen * A5_inv_mugen * vecNormInf(deltab5)
				/ vecNormInf(b5));
		System.out.println("(3)N=10について");
		System.out.println(A10_mugen * A10_inv_mugen * vecNormInf(deltab10)
				/ vecNormInf(b10));
		System.out.println("(3)N=15について");
		System.out.println(A15_mugen * A15_inv_mugen * vecNormInf(deltab15)
				/ vecNormInf(b15));

	}
}
