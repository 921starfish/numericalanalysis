package k8;

public class HilbertTest2and4 {

	public static void main(String[] args) {
		// 演習課題4

		// Hilbert行列の作成
		double[][] A5 = new double[5][5];
		double[][] A10 = new double[10][10];
		double[][] A15 = new double[15][15];

		for (int i = 0; i < 5; i++) {
			for (int j = 0; j < 5; j++) {
				A5[i][j] = 1.0 / ((i + 1) + (j + 1) - 1);
			}
		}
		for (int i = 0; i < 10; i++) {
			for (int j = 0; j < 10; j++) {
				A10[i][j] = 1.0 / ((i + 1) + (j + 1) - 1);
			}
		}
		for (int i = 0; i < 15; i++) {
			for (int j = 0; j < 15; j++) {
				A15[i][j] = 1.0 / ((i + 1) + (j + 1) - 1);
			}
		}

		double[][] tempA5 = new double[A5.length][A5[0].length];
		for (int i = 0; i < A5.length; i++) {
			for (int j = 0; j < A5[0].length; j++) {
				tempA5[i][j] = A5[i][j];
			}
		}
		double[][] tempA10 = new double[A10.length][A10[0].length];
		for (int i = 0; i < A10.length; i++) {
			for (int j = 0; j < A10[0].length; j++) {
				tempA10[i][j] = A10[i][j];
			}
		}
		double[][] tempA15 = new double[A15.length][A15[0].length];
		for (int i = 0; i < A15.length; i++) {
			for (int j = 0; j < A15[0].length; j++) {
				tempA15[i][j] = A15[i][j];
			}
		}

		System.out.println("事前準備の終了");
		// (2) Ax=b
		double[] b5 = new double[5];
		double[] b10 = new double[10];
		double[] b15 = new double[15];

		for (int i = 0; i < 5; i++) {
			double sum = 0;
			for (int j = 0; j < 5; j++) {
				sum += A5[i][j];
			}
			b5[i] = sum;
		}

		for (int i = 0; i < 10; i++) {
			double sum = 0;
			for (int j = 0; j < 10; j++) {
				sum += A10[i][j];
			}
			b10[i] = sum;
		}
		for (int i = 0; i < 15; i++) {
			double sum = 0;
			for (int j = 0; j < 15; j++) {
				sum += A15[i][j];
			}
			b15[i] = sum;
		}

		System.out.println("(2)N=5について残差ノルムは");
		System.out.println(GaussWithPivotForEnsyu.gaussPivot(tempA5, b5));
		System.out.println("(2)N=10について残差ノルムは");
		System.out.println(GaussWithPivotForEnsyu.gaussPivot(tempA10, b10));
		System.out.println("(2)N=15について残差ノルムは");
		System.out.println(GaussWithPivotForEnsyu.gaussPivot(tempA15, b15));
		
		// 再度初期化
		// A
		for (int i = 0; i < A5.length; i++) {
			for (int j = 0; j < A5[0].length; j++) {
				tempA5[i][j] = A5[i][j];
			}
		}
		
		for (int i = 0; i < A10.length; i++) {
			for (int j = 0; j < A10[0].length; j++) {
				tempA10[i][j] = A10[i][j];
			}
		}
		
		for (int i = 0; i < A15.length; i++) {
			for (int j = 0; j < A15[0].length; j++) {
				tempA15[i][j] = A15[i][j];
			}
		}
		
		// b
		for (int i = 0; i < 5; i++) {
			double sum = 0;
			for (int j = 0; j < 5; j++) {
				sum += A5[i][j];
			}
			b5[i] = sum;
		}

		for (int i = 0; i < 10; i++) {
			double sum = 0;
			for (int j = 0; j < 10; j++) {
				sum += A10[i][j];
			}
			b10[i] = sum;
		}
		for (int i = 0; i < 15; i++) {
			double sum = 0;
			for (int j = 0; j < 15; j++) {
				sum += A15[i][j];
			}
			b15[i] = sum;
		}
		
		double[] x5 = new double[5];
		double[] x10 = new double[10];
		double[] x15 = new double[15];

		for (int i = 0; i < x5.length; i++) {
			x5[i] = 1;
		}
		for (int i = 0; i < x10.length; i++) {
			x10[i] = 1;
		}
		for (int i = 0; i < x15.length; i++) {
			x15[i] = 1;
		}
		System.out.println("(2)N=5について誤差ノルムは");
		System.out.println(GaussWithPivotForEnsyu.gaussPivot(tempA5, b5, x5));
		System.out.println("(2)N=10について誤差ノルムは");
		System.out.println(GaussWithPivotForEnsyu.gaussPivot(tempA10, b10, x10));
		System.out.println("(2)N=15について誤差ノルムは");
		System.out.println(GaussWithPivotForEnsyu.gaussPivot(tempA15, b15, x15));
		
		// (4)
		// 再度初期化
		for (int i = 0; i < A5.length; i++) {
			for (int j = 0; j < A5[0].length; j++) {
				tempA5[i][j] = A5[i][j];
			}
		}
		
		for (int i = 0; i < A10.length; i++) {
			for (int j = 0; j < A10[0].length; j++) {
				tempA10[i][j] = A10[i][j];
			}
		}
		
		for (int i = 0; i < A15.length; i++) {
			for (int j = 0; j < A15[0].length; j++) {
				tempA15[i][j] = A15[i][j];
			}
		}
		
		// b
		for (int i = 0; i < 5; i++) {
			double sum = 0;
			for (int j = 0; j < 5; j++) {
				sum += A5[i][j];
			}
			b5[i] = sum;
		}

		for (int i = 0; i < 10; i++) {
			double sum = 0;
			for (int j = 0; j < 10; j++) {
				sum += A10[i][j];
			}
			b10[i] = sum;
		}
		for (int i = 0; i < 15; i++) {
			double sum = 0;
			for (int j = 0; j < 15; j++) {
				sum += A15[i][j];
			}
			b15[i] = sum;
		}

		// x
		for (int i = 0; i < x5.length; i++) {
			x5[i] = 1;
		}
		for (int i = 0; i < x10.length; i++) {
			x10[i] = 1;
		}
		for (int i = 0; i < x15.length; i++) {
			x15[i] = 1;
		}
		
		// bに誤差を混入させる。
		b5[0] += 0.001 * b5[0];
		b10[0] += 0.001 * b10[0];
		b15[0] += 0.001 * b15[0];
		
		System.out.println("(4)N=5について誤差ノルムは");
		System.out.println(GaussWithPivotForEnsyu.gaussPivot(tempA5, b5, x5));
		System.out.println("(4)N=10について誤差ノルムは");
		System.out.println(GaussWithPivotForEnsyu.gaussPivot(tempA10, b10, x10));
		System.out.println("(4)N=15について誤差ノルムは");
		System.out.println(GaussWithPivotForEnsyu.gaussPivot(tempA15, b15, x15));

	}

}
